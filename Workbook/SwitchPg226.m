% filename   SwitchPg226.m
% this scripts in an example in Palm, Intro to Matlab 7 for Engr, Pg. 226
% there are errors in how it handles strings.
% V. Bertsch
% 3/14/07

t = 0:100; x = exp(-t).*sin(t);
response = input('Type min, max, or sum.','s');
response = lower(response);
switch response
	case 'min'
        minimum = min(x)
	case 'max'
        maximum = max(x)
	case 'sum'
        total = sum(x)
    otherwise
        disp('You have not entered a proper choice.')
end
