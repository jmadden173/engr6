%-----------------
% Widgets Example
%-----------------
Wages = [5,5.5,6.5,6,6.25];
Hours = [40,43,37,50,45];
Output = [1000, 1100, 1000, 1200, 1100];

Widgets = sum(Output);
Salaries = Wages .* Hours;
TotalSalary = sum(Salaries);
Cost = (Output ./ Salaries);
TotalCost = sum(Cost);

% Efficiency (output/cost)
%--------------------------
EfficiencyOC = Output ./ Wages;
max(EfficiencyOC);

% Efficiency (output/hour)
%--------------------------
EfficiencyOH = Output ./ Hours;
max(EfficiencyOH);

%------------------
% Railroad Example
%------------------
Tankers = [14, 30; 12, 35; 10, 40; 9.5, 40];
Inventory = [10,4,19,3];

TankerVolume = Tankers(:,1).*pi.*(Tankers(:,2).^2);
TotalTankerVolume = sum(TankerVolume);