rng('shuffle');

% Create all my colors
DEFAULT = [.94 .94 .94];
RED = [.64 .08 .18];
GREEN = [.47 .67 .19];
BLUE = [0 0.45 .74];
YELLOW = [1 1 0];

FROZEN = YELLOW;

gameState = zeros(1,5);

rollLimit = get(findobj('tag', 'RollsSL'), 'Value');