set(findobj('tag', 'RollsSL'), 'Visible', 'Off');
set(findobj('tag', 'RollsTB'), 'Visible', 'Off');
set(findobj('tag', 'NameText'), 'Visible', 'Off');
set(findobj('tag', 'NameEB'), 'Visible', 'Off');
set(findobj('tag','RollsLeftTB'), 'Visible', 'On')

if rollLimit == 0
    set(findobj('tag', 'RollPB'), 'Enable', 'off');
end

set(findobj('tag','RollsLeftTB'), 'String', ['Rolls Left: ', num2str(rollLimit)]);
rollLimit = rollLimit-1;

diceRoll = randi(6, [1,5]);

for i=1:5
    dieHandle = findobj('tag', ['die', num2str(i)]);
    set(dieHandle, 'Enable', 'on');
    if get(dieHandle, 'BackgroundColor') ~= FROZEN
        gameState(i) = randi(6);
        set(dieHandle, 'string', num2str(gameState(i)));
    end
end

for i=1:6
    gameStateCheck = ones(1,5) * i;
    if gameStateCheck == gameState
        open yahtzeeCongrats.fig
        break
    end
end