if get(gcbo, 'BackgroundColor') == FROZEN
    set(gcbo, 'BackgroundColor', DEFAULT);
else
    set(gcbo, 'BackgroundColor', FROZEN);
end