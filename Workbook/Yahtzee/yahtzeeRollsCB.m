rollLimit = round(get(gcbo, 'Value'));
set(gcbo, 'Value', rollLimit);

set(findobj('tag', 'RollsTB'),'string', ['Rolls: ', num2str(rollLimit)]);

set(findobj('tag','RollsLeftTB'), 'String',['Rolls Left: ', num2str(rollLimit)]);