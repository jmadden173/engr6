% filename:  Savings.m
% purpose:   Computes savings account balances by compounding the interest daily.
%            Uncompounded interest rate is divided by 365 to compute each new balance.
%            Ignores leap year.
% inputs:    Balance, scalar, beginning account balance.
%            Rate, scalar, yearly uncompounded interest rate, i.e. 2.2 is 2.2% or .022
%            Years, scalar, the number of years to continue the simulation.
% outputs:   Balance, scalar, the amount of money in the account.
% V. Bertsch
% 10/26/16

Balance = 1000;
Rate = 2.2;
Years = 15;

% for i=1:365*Years
%     Balance = Balance*(1 + (2.2/100/365));
% end

for i=1:365*Years
    Balance = [Balance, Balance(end)*(1 + (2.2/100/365))];
end

Balance










