% filename:  DeansList.m
% purpose:   The script asks the user to select the excel file that contains
%            the names and GPA info in it.  The script then selects scholars 
%            that have GPA's above the 3.8 cutoff.
% inputs:    ExcelGpaFile, string, filename selected by user.
% outputs:   Scholars, cell array of strings, column of names for the dean's list.
% reference: Engr 6, Matlab Programming
% V. Bertsch
% 10/11/12

Cutoff = 3.80;  % used to select Scholars
%ExcelGpaFile = uigetfile('*.xlsx');
[ ~ , ~ , StudentData ] = xlsread('N:\\ENGINEERING\\Engr6\\GPAsS13.xlsx');

Names = StudentData(:,2:3)

GPAs = [StudentData{:,5}]

Scholars = Names(GPAs >= Cutoff)


% clear Cutoff ExcelGpaFile Names GPAs StudentData