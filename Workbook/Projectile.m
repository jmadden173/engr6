% filename:   Projectile.m
% purpose:    Simulation to chart height and velocity of a projectile
% reference:  Palm, Intro to Matlab 7, Ch 4, Prob 12, pg. 243
% inputs:     Vo, scalar, initial total velocity in meters/second
%             Theta, scalar, initial trajectory angle in degrees
% state vars: Height, building vector, projectile heights
%             VVel, building vector, vertical component of velocity
% state rela: Euler method equations:  H = Ho + V*dt, V = Vo + a*dt
% interval:   DeltaT, scalar, simulation time interval between iterations
% V. Bertsch
% 12/17/08

Vo = 40;     % m/s
Theta = 30;  % degrees
g = 9.81;    % m/s^2

VVel = Vo*sind(Theta);
Height = 0;
DeltaT = .1;
for TIndex = 1:5/DeltaT
    Height(TIndex+1) = Height(TIndex) + VVel(TIndex)*DeltaT;
    VVel(TIndex+1) = VVel(TIndex) - g*DeltaT;
end

plot(1:5/DeltaT+1, Height, 1:5/DeltaT+1, VVel)

