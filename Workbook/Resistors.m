% filename:  
% purpose:   To compute equivalent resistance for series or parallel
%               circuts.
% inputs:    Resist, numerical array, values for the resistors in ohms.
%            ConType, scalar character, s for series & p for parallel
% output:    Req, numerical scalar, value of equivilant resistance
% reference: in class exercise, SRJC, ENGR 6, Fall 2017
% John Madden
% 10/1/2018

con = input('Input the connection type (p/s): ', 's');
resist = input('Input a vector of resistors: ');

if (con == 'p' || con == 'parallel')
   totresist = sum(1./resist);
   disp(['The total resistance for a parrallel circuit is: ', num2str(totresist)]);
elseif (con == 's' || con == 'series')
    totresist = sum(resist);
    disp(['The total resistance for a series circuit is: ', num2str(totresist)]);
else
    disp('Please only enter s or p for connection type');
end

