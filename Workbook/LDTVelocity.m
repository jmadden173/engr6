% Filename:  LDTVelocity.m
% This script computes and plots the Lift and Drag as a function of velocity
% using coefficients equations derived from wind tunnel tests.  The script also
% computes the propeller thrust as a function of velocity using manufacturers data.
% Plot compares thrust and drag vs. velocity.
% V. Bertsch
% 10/11/12

Alpha = 10;                 % angle of attack in degrees
AirDensity = .002378;       % at sealevel, slug/ft^3
WingSpan = 36;              % in feet

VelMPH = [0:.1:150];        % velocities in miles per hour
VelFPS = VelMPH*5280/3600;  % velocities convered to ft/sec

CL = 4.47E-5*Alpha.^3 + 1.15E-3*Alpha.^2 + 6.66E-2*Alpha + 1.02E-1;
CD = 5.75E-6*Alpha.^3 + 5.09E-4*Alpha.^2 + 1.81E-4*Alpha + 1.25E-2;

Lift = .5*AirDensity*CL*WingSpan*VelFPS.^2;       % in pounds, Velocity in ft/sec
Drag = .5*AirDensity*CD*WingSpan*VelFPS.^2;       % in pounds, Velocity in ft/sec
Thrust = 150*exp(-VelFPS/40);                     % in pounds, Velocity in ft/sec

plot(VelMPH, Thrust, 'm', VelMPH, Drag, 'r--')
% or plot(VelMPH, [Thrust;Drag])

title('Ultralight Aircraft Flight Characteristics')
xlabel('Velocity (mi/hr)')
ylabel('Thrust & Drag (pounds)')
text(0,157, {'V. Bertsch';'10/11/12'}, 'horizontalalignment','right')
legend({'Thrust','Drag'},'Position', [.4,.5, .2, .1])

% 5. compute the speeds for which Thrust is greater than 100 lbs or Drag is less than 20.
VelMPH(Thrust > 100 | Drag < 20)

% 6. compute the maximum speed (when Thrust = Drag).
max(VelMPH(Thrust >= Drag-.1 & Thrust <= Drag+.1))