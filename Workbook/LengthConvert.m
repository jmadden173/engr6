% filename:   LengthConvert.m
% purpose:    This script converts an entered length (any units) to other units.
% inputs:     user enters length
%             user chooses original units from menu
%             user chooses new units from menu
% outputs:    screen display of converted length (with new units)

Units = {'Inches',        1 ;
         'Feet',        1/12 ;
         'Yards',       1/36 ;
         'Centimeters', 2.54 ;
         'Millimeters', 25.4 ;
         'Meters',     .0254     };

OldLength = input('Enter the length>>');

OldUnits = menu('Specify the original units', Units{:,1});

disp( ['You have entered ', num2str(OldLength)])

NewUnits = menu('Specify the new units', Units{:,1});

NewLength = OldLength / (Units{NewUnits,2} / Units{OldUnits,2});

disp( ['which is equal to ', num2str(NewLength),' ', Units{NewUnits,1} ,'.'])
