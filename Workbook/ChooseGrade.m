% filename:  ChooseGrade.m
% this scripts an example of using a switch with a scalar string comparison.
% V. Bertsch
% 10/16/05

% Choice = input('What grade do you want in Engr 6>>','s');

Choice = menu('What grade do you want in Engr 6', 'A', 'B', 'C', 'D', 'E', 'F');

switch Choice
	case 1
		disp('Plan for more than 6 hours per week of work outside of class.')
    case 2
		disp('Plan for about 6 hours per week of work outside of class.')
	case {3,4,5}
		disp('Naps in class are so refreshing.')
	otherwise
		disp('Don''t be a wiseguy.')
end
