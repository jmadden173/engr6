n=2;

aprox = (1-(1/n))^n;

while abs(aprox - exp(-1)) > 0.0001
    aprox = (1-(1/n))^n;
    n=n+1;
end