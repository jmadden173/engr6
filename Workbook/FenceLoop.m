% filename:   FenceLoop.m
% purpose:    Optimization of radius and length of corral.
% reference:  Palm, Intro to Matlab 7, Ch 2, Prob 20, pg. 129
% inputs:     Area, scalar, area of corral in square feet (1600)
%             CurveCost, scalar, cost in $ per foot for curved sections (40)
%             StraightCost, scalar, cost in $ per foot for straight sections (30)
% Outputs:    BestCost, scalar, cost of lowest price fence
%             BestR, scalar, optimum radius R in feet
%             BestL, scalar, optimum length L in feet
% V. Bertsch
% 6/18/07

Area = 1600;   % square feet
CurveCost = 40;     % $/ft
StraightCost = 30;  % $/ft

BestCost = Inf;

for Rad = 0:.1:(2*Area/pi)^.5
    Len = (Area - 1/2*pi*Rad^2)/(2*Rad);
    CurrentCost = (2*Rad+2*Len)*StraightCost + pi*Rad*CurveCost;
    
    if (CurrentCost < BestCost)
        BestCost = CurrentCost;
    end
end


