% filename:  LDAngle.m
% this script computes the coefficients of lift and drag using equations
% from wind tunnel testing and plots the lift/drag ratio verses angle of attack.
% V. Bertsch
% 10/11/12

Alpha = -20:.01:20;           % angle of attack in degrees

CL = 4.47E-5*Alpha.^3 + 1.15E-3*Alpha.^2 + 6.66E-2*Alpha + 1.02E-1;
CD = 5.75E-6*Alpha.^3 + 5.09E-4*Alpha.^2 + 1.81E-4*Alpha + 1.25E-2;
LDRatio = CL./CD;

plot(Alpha, LDRatio)

% 1. compute the maximum lift/drag ratio 
[MaxRatio, MaxRatioIndex] = max(LDRatio)

% 2. compute the angle corresponding to the maximum lift/drag ratio.
Alpha(MaxRatioIndex)

% 3. compute the angles for which lift/drag ratio is greater than 14.
Alpha(LDRatio > 14)

% 4. compute the angles for which lift/drag ratio is between 5 and 14.
Alpha(LDRatio < 14 & LDRatio > 5)

