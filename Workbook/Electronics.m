% filename  Electronics.m
% Palm, Intro to Matlab for Engineers, Ch 4 Prob 30
% Optimization of assembly of electronic components into electronic equipment
% V. Bertsch
% 4/8/15

clear;

Inventory = [450 250 800 450 600]';  % of Chassis, Tube, Cone, Supply, Electr.
UnitProfit = [80 50 40];             % for Television, Stereo, Speaker
Require = [1 1 0; 1 0 0; 2 2 1; 1 1 0; 2 2 1];
StepSize = 1;
MaxProfit = 0;
Solution = [];
tic
for TV = 0:StepSize:min(Inventory./Require(:,1)) % 250
   for St = 0:StepSize:min(Inventory./Require(:,2))   %300
      for Sp = 0:StepSize:min(Inventory./Require(:,3))  %600
            if  Require*[TV St Sp]' <= Inventory  % if has built in all
               Profit = [TV St Sp]*UnitProfit';    
               if Profit > MaxProfit
                  MaxProfit = Profit;
                  Solution = [TV St Sp Profit];
               elseif Profit == MaxProfit
                  Solution = [Solution ; [TV St Sp Profit]];
               end
            end
      end
   end
end
toc
disp('Optimum number of widgets')
disp(' Televisions  Stereos   Speakers   Net Profit')
disp(Solution)


