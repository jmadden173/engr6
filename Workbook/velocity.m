Mass = 5; % in kg
Vel = [0,10,0];
InitPos = [2,3,0];
Time = 0:0.5:5;

TimeLen = length(Time);

Pos = repmat(Vel, TimeLen, 1) .* Time' + repmat(InitPos, TimeLen, 1);

L = Mass .* cross(Pos, repmat(Vel, length(Pos), 1))