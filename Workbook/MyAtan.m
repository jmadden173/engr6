function output = MyAtan(x)
    output = 0;
    for k=0:1000
       output = output + (((-1)^2)/(2*k + 1)) * (x^(2*k + 1));
    end
end