function [minL,minR,minCost] = corral(A)
    R = 0:.01:sqrt((2*A)/pi);
    L = (A-((pi.*(R.^2))./2))./(2.*R);
    cost = ((40*pi).*R) + (30.*(2.*R+2.*L));
    [minCost, minCostIndex] = min(cost);
    
    minL = L(minCostIndex);
    minR = R(minCostIndex);
end