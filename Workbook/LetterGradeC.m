% Filename:     LetterGradeC.m
% Purpose:      Determine the letter grade based on the students 0-100 score.
% Inputs:       Score, scalar number, student's percent grade, user input.
% Outputs:      Grade, scalar character, students letter grade, out to screen.
% V. Bertsch
% 3/22/08

GradeScale = { 'A', 90 ;
               'B', 80 ;
               'C', 70 ;
               'D', 60 ;
               'F', -Inf }  ;
           
Score = input('Please enter the numerical score (between 0 and 100) >>  ');

Row = find(Score >= [GradeScale{:,2}], 1 );

Grade = GradeScale{Row,1};

disp(['The student''s letter grade is ',Grade,' .'])
