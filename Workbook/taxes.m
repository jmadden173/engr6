function [liability] = taxes(income)
    if (income >= 0 && income <= 2000)
        liability = 0;
    elseif (income > 2000 && income <= 4000)
        liability = 0.05 * income;
    elseif (income > 4000 && income >= 6000)
        liability = 100 + .08 * income;
    elseif (income > 6000)
        liability = 260 + .10 * income;
    else
        liability = -1;
    end
end