% filename:   Cheer.m
% purpose:    Demonstrate nexted for loops to spell out names in a cheer.
% inputs:     Names, Cell array of strings, names of the people to cheer for.
% outputs:    display to screen to prompt the cheer.
% V. Bertsch
% 4/2/18

Names = { 'Jan' , 'Alex' , 'Vince' };

for PersonIndex = 1:length(Names)
    
    for LetterIndex = 1:length(Names{PersonIndex})
        
        
        disp( ['Give me a ', Names{PersonIndex}(LetterIndex)] );
            
        pause(1.5)
    end
    
    disp('What''s that spell?')
    
    pause(1.5)
end
