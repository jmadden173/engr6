%   filename:   MakeStep5.m
%   purpose:    The script file makes random step response data for a 4 axis
%               missile control actuator system for testing the data processing software.
%   input:      Cycles, scalar number, the number of times each axis is sampled.
%               SampleRate, scalar, time required for sampling one axis of data.
%   output:     StepData, numerical array, dummy numbers in a step response data array. 
%                   1st column is axis number (cycles through 1,2,3,4)
%                   2nd column is time stamp (about i millisecond per axis)
%                   3rd column is command value (random number -1 to 1)
%                   4th column is potentiometer value (random number -1 to 1)
%                   5th column is encoder value (random number -1 to 1)
%                   6th column is PWM value (random number -1 to 1)
%                   7th column is motor current (random number -1 to 1)
%   reference:  TSM Project, Bogus Aerospace
%   V. Bertsch  
%   10/11/12 

function StepData = MakeStep(SampleRate,Cycles) 
    %generate axis numbers
    Axis([1:4:4*Cycles])=1;
    Axis([2:4:4*Cycles])=2;
    Axis([3:4:4*Cycles])=3;
    Axis([4:4:4*Cycles])=4;

    %generate time stamps
    TimeStamps=SampleRate*[1:4*Cycles];

    %generate random feedback values
    Feedback=rand(4*Cycles,5)*2-1;

    %combine to make step data
    StepData=[Axis' TimeStamps' Feedback];

    clear Axis Feedback TimeStamps
end





