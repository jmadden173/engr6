% x = -2:0.25:2;
% [X,Y] = meshgrid(x);
% Z = X.*exp(-X.^2-Y.^2);
% contour3(X,Y,Z,100)

% X = randi([-10,10],10)
% contour3(X,100);

% A = sin(-pi:.1:pi);
% B = repmat(A,10,1)
% contour3(B,100)

[X,Y,Z] = peaks(25);
surf(X,Y,Z)