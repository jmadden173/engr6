% Filename: MedicationLevels.m
% Purpose:  Compute and plot concentration values for
%           a range of metabolic rates k
% Output:   Plot of 
% Reference:SRJC, ENGR 6, Matlab Programming, Inclass example
% Author:   John Madden
% Date:     9/10/2018

k = .047:.001:.107;
a = 1;
t = 1:10;

C = (a./k).*(1-exp(-1.*k.*t'))

plot(k,C)