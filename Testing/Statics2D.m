% Filename: Statics2D.m
% Purpose:  Compute and plot forces for a range of rope angles.
% Inputs:   Mass, numerical scalar, mass of the hanging weight.
%           g, numerical scalar, gravitational constant (earth surface).
%           Theta, numerical vector, range of angles in degrees.
% Outputs:  Force, numerical vector, forces for each angle in Newtons
%           script also output a plot of Force vs Angle (Theta)
% Reference:SRJC, ENGR 6, Matlab Programming, Inclass example
% Author:   Vince Bertsch
% Date:     9/10/2018

Mass = 15; % (kg)
g = 9.80; % (m/s^2)

Theta = 0:1:85;     % (degrees)

Force = (Mass*g)./(2.*cosd(Theta));    % (N)

plot(Theta, Force)