tic

rng('shuffle');

rolls = 1;
ndice = 2;

profit = 0;

for players = 1:100
    for games = 1:100
        outcome = zeros(1,rolls,ndice);
        for i = 1:ndice
            outcome(:,:,i) = randi(6,1,rolls);
        end
        covalue = sum(outcome, 3);

        % Come-out phase
        if covalue == 2 || covalue == 3 || covalue == 12
            profit = profit + 5;
        elseif covalue == 7 || covalue == 11
            profit = profit - 5;
        else
            % Point Phase
            while (1)
                outcome = zeros(1,rolls,ndice);
                for i = 1:ndice
                    outcome(:,:,i) = randi(6,1,rolls);
                end
                value = sum(outcome, 3);

                if value == covalue
                    profit = profit - 5;
                    break;
                elseif value == 7
                    profit = profit + 5;
                    break;
                end
            end
        end
    end
end

profit

toc