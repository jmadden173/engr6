function [ outcome ] = die( rolls, ndice )
    rng('shuffle');
    outcome = zeros(1,rolls,ndice);
    for i = 1:ndice
        outcome(:,:,i) = randi(6,1,rolls);
    end
    
    hist(sum(outcome, 3),ndice:ndice*6);
end
