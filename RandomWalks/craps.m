rng('shuffle');

rolls = 1;
ndice = 2;

outcome = zeros(1,rolls,ndice);
for i = 1:ndice
    outcome(:,:,i) = randi(6,1,rolls);
end
covalue = sum(outcome, 3);

% Come-out phase
if covalue == 2 || covalue == 3 || covalue == 12
    fprintf('Craps, the shooter loses!\n');
elseif covalue == 7 || covalue == 11
    fprintf('Natural, the shooter wins!\n');
else
    % Point Phase
    while (1)
        outcome = zeros(1,rolls,ndice);
        for i = 1:ndice
            outcome(:,:,i) = randi(6,1,rolls);
        end
        value = sum(outcome, 3);
        
        if value == covalue
            fprintf('The shooter wins!\n');
            break;
        elseif value == 7
            fprintf('The shooter loses!\n');
            break;
        end
    end
end
    