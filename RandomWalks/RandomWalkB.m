clear

rng('shuffle');
p = 0.5;
loop = 100;
particles = 100000;
counter = zeros(1,particles);

for j = 1:particles
    for i = 1:loop
        if rand < p
            counter(j) = counter(j) + 1;
        else
            counter(j) = counter(j) - 1;
        end
        %plot(counter(i), 0, 'o'), axis([-20 20 -1 1]), grid;
    end
    %hist(counter, min(counter):2:max(counter))
    %axis([-1 1 0 loop])
    %pause(3/loop);
end
hist(counter, min(counter):2:max(counter))