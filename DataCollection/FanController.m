clear;
Session = daq.createSession('ni');

Session.addAnalogInputChannel('myDAQ1','ai0','Voltage')
addDigitalChannel(Session, 'myDAQ1', 'port0/line0', 'OutputOnly');
addDigitalChannel(Session, 'myDAQ1', 'port0/line1', 'InputOnly');

while 1
    input = Session.inputSingleScan
    if (input(1) < 2.35)
        outputSingleScan(Session, 1);
    else
        outputSingleScan(Session, 0);
    end
    
    if ~input(2)
        break;
    end
end