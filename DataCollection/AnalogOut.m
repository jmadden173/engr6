clear;
Session = daq.createSession('ni');

% analog output
Session.addAnalogOutputChannel('myDAQ1','ao0','Voltage')
Session.outputSingleScan(.1)

ap_volt = (sin(0:(pi/100):2*pi)+1).*(2/10);

while 1
    for i=1:length(ap_volt)
        Session.outputSingleScan(ap_volt(i)+1.3);
    end
end