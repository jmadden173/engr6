clear;
Session = daq.createSession('ni');

% analog output
Session.addAnalogOutputChannel('myDAQ1','ao0','Voltage')
Session.addAnalogInputChannel('myDAQ1','ai0','Voltage')

ap_volt = sin(0:(pi/50):2*pi)+1;
ac_volt = zeros(1, length(ap_volt));

for i=1:length(ap_volt)
    Session.outputSingleScan(ap_volt(i));
    ac_volt(i) = Session.inputSingleScan;
end

% plot(length(ap_volt), ap_volt, length(ac_volt), ac_volt);
plot(1:length(ap_volt), ap_volt, 1:length(ac_volt), ac_volt, '--');

Session.outputSingleScan(0);