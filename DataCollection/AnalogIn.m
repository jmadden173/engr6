clear;
Session = daq.createSession('ni');

% analog output
Session.addAnalogOutputChannel('myDAQ1','ao0','Voltage')
Session.addAnalogInputChannel('myDAQ1','ai0','Voltage')

Session.outputSingleScan(1);

Session.inputSingleScan