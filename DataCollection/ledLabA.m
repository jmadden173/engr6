clear;

Session = daq.createSession('ni');
addDigitalChannel(Session, 'myDAQ1', 'port0/line1:3', 'OutputOnly');
outputSingleScan(Session, [0 0 0]);

pause;

outputSingleScan(Session, dec2bin(3));