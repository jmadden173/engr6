clear;

Session = daq.createSession('ni');
addDigitalChannel(Session, 'myDAQ1', 'port0/line1:3', 'OutputOnly');
while true
    outputSingleScan(Session, randi([0 1], 1, 3));
    pause(1);
end