tic
load('N:\\ENGINEERING\\Engr6\\EggsLoc.mat');

step = .01;
rx = min(EggLoc(:,1)):step:max(EggLoc(:,1));
ry = min(EggLoc(:,2)):step:max(EggLoc(:,2));
distance = [];
xs = [];
ys = [];

for x=rx
    for y=ry
        distance = [distance, sum(sqrt(((x - EggLoc(:,1)).^2) + ((y - EggLoc(:,2)).^2)))];
        xs = [xs, x];
        ys = [ys, y];
    end
end

[mindist, mindistIndex] = min(distance);
disp(['You should stand at (', num2str(xs(mindistIndex)), ',', num2str(ys(mindistIndex)), ') in order to minimize your distance of ', num2str(mindist),' from all eggs']);
toc