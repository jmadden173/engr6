function [digits, string] = splitAddress(address)
    afterspace = 0;
    digits = [];
    string = [];
    for i=address
        if i == ' ' && afterspace == 0
            afterspace = 1;
            continue
        end
        
        if afterspace == 0
            digits = [digits, i];
        else
            string = [string, i];
        end
    end
end