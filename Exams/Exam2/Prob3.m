shape = menu('Select Shape', 'Circle', 'Rectangle', 'Triangle');

switch shape
    case 1
        length = input('Enter radius of the circle: ');
        area = pi*(length.^2)
    case 2
        sides = zeros(1,shape);
        for i=1:shape
            sides(i) = input(['Enter side length of side #', num2str(i), ': ']);
        end
        area = sides(1) * sides(2)
    case 3
        base = input('Enter the length of the base: ');
        height = input('Enter the height of the triangle');
        area = (1/2) * base * height
end