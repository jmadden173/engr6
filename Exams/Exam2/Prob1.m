Vel = 20:10:70;
Dist = [45, 80, 130, 185, 250, 330];

plot(Dist, Vel);

title({'Plot of stopping distance vs initial velocity';'of a Toyota 4-Runner with bald tires'});

xlabel('Distance (feet)');
ylabel('Velocity (mph)');

axes('position',[0 0 1 1],'visible','off')
text(.95,.95,{'SRJC Engr 6';'John Madden';'10/24/2018'},...
   'HorizontalAlignment','right')
text(.20,.70,{'Vel (ft)';Vel},...
   'HorizontalAlignment','left')
text(.30,.70,{'Dist (mph)';Dist},...
   'HorizontalAlignment','left');