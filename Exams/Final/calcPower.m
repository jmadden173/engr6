function [totalsum, avg] = calcPower(powerData)
    weeksum = sum(powerData, 2);
    totalsum = sum(weeksum);
    [~, dataLength] = size(PowerData);
    avg = weeksum./dataLength;
end