function [reqBlocks] = calcPyramid (prevBlocks)
    reqBlocks = 0;
    while (prevBlocks > 0)
        reqBlocks = reqBlocks + prevBlocks;
        prevBlocks = round(prevBlocks * .9) - 4;
    end
end