% Prob3

principle = 10000;
years = 10;

interestpercent = .2:.2:3;
interest = interestpercent./100;

futurebal = principle.*(1+interest).^years;

disp('     Interest(%)   Future Bal')
disp([interestpercent', futurebal'])

