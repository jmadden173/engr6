while (1)
    fprintf('+---------------------------+\n');
    fprintf('| Enter in the type of acid |\n');
    fprintf('| 1) strong                 |\n');
    fprintf('| 2) weak                   |\n');
    fprintf('| 3) plot                   |\n');
    fprintf('+---------------------------+\n');

    
    switch input('>> ')
        case 1
            while (1)
                fprintf('Enter in the concentration of the acid [HA]\n');
                HA = input('>> ');
            
                if (HA >= 0)
                    break;
                else
                    fprintf('ERROR: Enter in valid [HA]\n');
                end
            end
            
            if (HA == 0)
                pH = 7;
            else
                pH = -1 * log(HA);
            end
            
            break;
            
            
            
            
        case 2
            while (1)
                fprintf('Enter in [HA] and K(a)\n');
                HA = input('[HA]>> ');
                Ka = input('K(a)>> ');
                
                if (HA >= 0 && Ka >=0)
                    break;
                else
                    fprinf('ERROR: Enter in valid [HA] and K(a)\n');
                end
            end

            if (HA == 0 || Ka == 0)
                pH = 7;
                break;
            else
                pH = ((-1 * Ka) + sqrt((-1 * Ka)^2 - 4*(-1*Ka*HA)))/2;
                pH = -1 * log(pH);
                break;
            end
            
            
            
        case 3
            while (1)
                fprintf('Enter in K(a)\n');
                Ka = input('K(a)>> ');
                
                if (Ka >= 0)
                    break;
                else
                    fprintf('ERROR: Enter in value K(a)\n');
                end
            end
            
            HA = 0.01:0.01:1.0;
            
            pH = ((-1 .* Ka) + sqrt((-1 .* Ka).^2 - 4.*(-1.*Ka.*HA)))./2;
            pH = -1 * log(pH);
            
            break;
        
        otherwise
            fprintf('Please Eenter strong or weak\n');
    end
end

fprintf('The acid has a pH level of %f\n', pH);
plot(HA, pH);