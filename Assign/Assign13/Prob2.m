% File:         Prob2.m
% Description:  Computes the price to the product "CoatAll"
%               for a set of fixed cost and variable costs.
%               Generates a plot as well corresponding to
%               the most profits
% Output:       Displays a plot of the price vs profit
%               along with other information
% Author:       John Madden
% Date:         10/15/2018

CONST_MAT = .62; % in cents per gallon
CONST_ENGERGY = .24; % in cents per gallon
CONST_LABOR = .16; % in cents per gallon
CONST_ANNUAL_EXPENSES = 2.045; % million per year

% price goes from the minimum cost to profit to
% where the quanity was 0
expenses = CONST_MAT + CONST_ENGERGY + CONST_LABOR;
price = expenses:.01:(-6 / -1.1);
quantity = 6 - (1.1 .* price);

profit = (price .* quantity) - (expenses .* quantity) - CONST_ANNUAL_EXPENSES;

[maxProfit, maxProfitIndex] = max(profit);


breakEvenMin = find(profit > 0, 1, 'first');
breakEvenMax = find(profit > 0, 1, 'last');

%plot(price,profit,price,zeros(length(price),1));

% Create figure
figure1 = figure;

% Create axes
axes1 = axes('Parent',figure1);
hold(axes1,'on');

% Create multiple lines using matrix input to plot
plot(price,profit,price,zeros(length(price),1));

% Create xlabel
xlabel({'Price (in millions of dollars)'});

% Create title
title({'Graph of Quantity vs Price for "CoatAll"'});

% Create ylabel
ylabel({'Profit (in millions of dollars)'});

box(axes1,'on');
% Create textarrow
annotation(figure1,'textarrow',[0.546428571428571 0.510714285714286],...
    [0.75 0.845238095238095],'String',{'Max profit at', price(maxProfitIndex)});

% Create textarrow
annotation(figure1,'textarrow',[0.258928571428571 0.223214285714286],...
    [0.361904761904762 0.447619047619048],'String',{'Break even at',price(breakEvenMin)});

% Create textarrow
annotation(figure1,'textarrow',[0.751785714285714 0.808928571428571],...
    [0.35852380952381 0.440476190476191],'String',{'Break even at',price(breakEvenMax)});

% Create textbox
annotation(figure1,'textbox',...
    [0.0105879120879118 0.890476190476194 0.203697802197802 0.0934784068775617],...
    'String',{'Paint-R-Us','Profit on "CoatAll'},...
    'FitBoxToText','off');

% Create textbox
annotation(figure1,'textbox',...
    [0.810714285714285 0.902380952380953 0.175 0.0955604808919437],...
    'String',{'John Madden','10/15/2018'},...
    'FitBoxToText','off');

% Create textbox
annotation(figure1,'textbox',...
    [0.425423076923076 0.433333333333334 0.188862637362638 0.0573241990731448],...
    'String',{'Break even line'},...
    'FitBoxToText','off',...
    'BackgroundColor',[1 1 1]);

% Create textbox
annotation(figure1,'textbox',...
    [0.713472527472528 0.695238095238095 0.186527472527472 0.185133319900598],...
    'String',{'Q = 6 - 1.1 * P',sprintf('Materials: %0.2f',CONST_MAT),sprintf('Energy: %0.2f',CONST_ENGERGY),sprintf('Labor: %0.2f', CONST_LABOR)},...
    'FitBoxToText','off');

