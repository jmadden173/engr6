% File:         Prob1.m
% Description:  Determines how long it will take in order to
%               accumulate at least 1 million dolors in a bank
%               account starting with $10,000 and deposit
%               $10,000 at the end of each year with 6% annual
%               interest
% Output:       count, the amount of years to reach the cutoff
%               bal, the total balence of the account
% Author:       John Madden
% Date:         10/15/2018

CONST_CUTOFF = 10^9;
CONST_DEPOSIT = 10^4;
CONST_INTEREST = 0.06;

bal = 10000;
counter = 0;

while bal < CONST_CUTOFF
   bal = bal*(1+CONST_INTEREST);
   bal = bal + CONST_DEPOSIT;
   counter = counter + 1;
end

disp(['The total balence is ', num2str(bal), ' and it was reached after ', num2str(counter), ' years.']);