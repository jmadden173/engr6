% Program: Prob3_9.m
% Date: 8/29/2018
% Author: John Madden

% Part A
% Store values into A
A = [3,4,2;2,4,100;7,9,7;3,pi,42]

% Take ln of A
B = log(A)

% Sum of the first row ./ by the first
% 3 elements in the third column
sum(A(1,:) ./ B(1:3,3)')


% Prob3_9
% A =
%     3.0000    4.0000    2.0000
%     2.0000    4.0000  100.0000
%     7.0000    9.0000    7.0000
%     3.0000    3.1416   42.0000
% B =
%     1.0986    1.3863    0.6931
%     0.6931    1.3863    4.6052
%     1.9459    2.1972    1.9459
%     1.0986    1.1447    3.7377
% ans =
%      6.2245