% Program: Prob3_1.m
% Date: 8/29/2018
% Author: John Madden

% Create a vector X using : method
X = 8:(30-8)/(15-1):30

% Create a vector X using 
X = linspace(8,30,15)


% Prob3_1
% X =
%     8.0000    9.5714  ... 28.4286   30.0000
% X =
%     8.0000    9.5714  ... 28.4286   30.0000