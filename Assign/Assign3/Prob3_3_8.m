% Program: Prob3_3-8.m
% Date: 8/29/2018
% Author: John Madden

% Create array with values
A = [3,7,-4,12;-5,9,10,2;6,13,8,11;15,5,4,1]

% Create B from columns 2-4 of A
B = A(:,2:4)

% Create empty array with same row and columns as A
C = zeros(size(A))

% Sort Every row of A
D = sort(A,2)

% Sort rows by column 3 and in decending order
% Keeps data intact
E = sortrows(A,3,'descend')

% Find max values as well as index
[AMax, AMaxIndex] = max(A)


% Prob3_3_8
% A =
%      3     7    -4    12
%     -5     9    10     2
%      6    13     8    11
%     15     5     4     1
% B =
%      7    -4    12
%      9    10     2
%     13     8    11
%      5     4     1
% C =
%      0     0     0     0
%      0     0     0     0
%      0     0     0     0
%      0     0     0     0
% D =
%     -4     3     7    12
%     -5     2     9    10
%      6     8    11    13
%      1     4     5    15
% E =
%     -5     9    10     2
%      6    13     8    11
%     15     5     4     1
%      3     7    -4    12
% AMax =
%     15    13    10    12
% AMaxIndex =
%      4     3     2     1
