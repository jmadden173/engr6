% Program: Prob3_10.m
% Date: 8/29/2018
% Author: John Madden

% Part A
% ------
% Force in Newtons
Force = [400,550,700,500,600];
% Distance in Meters
Distance = [3,0.5,0.75,1.5,5];

% Compute Work
Work = Force .* Distance

% Part B
% ------
% Get total work
TotalWork = sum(Work)


% Prob3_10
% Work =
%         1200         275         525         750        3000
% TotalWork =
%         5750