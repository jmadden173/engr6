% Program: Prob3_11.m
% Date: 8/29/2018
% Author: John Madden

% Part A
% -------
% Force in Newton
Force = [11,7,8,10,9];
% SpringConstants in Newtons/Meter
SpringConstants = [1000,600,900,1300,700];

% Solve for compression
Compression = Force .* SpringConstants

% Part B
% ------
% Solve for PE using kx^2/2
PotentialEnergy = (SpringConstants.*(Force.^2))/2


% Prob3_11
% Compression =
%        11000        4200        7200       13000        6300
% PotentialEnergy =
%        60500       14700       28800       65000       28350