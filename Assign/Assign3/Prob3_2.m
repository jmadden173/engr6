% Program: Prob3_2.m
% Date: 8/29/2018
% Author: John Madden

% Create Y with logspace()
Y = logspace(1,3,20)


% Prob3_2
% Y =
%    1.0e+03 *
%     0.0100    0.0127  ... 0.7848    1.0000