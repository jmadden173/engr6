% Proab6_2.m

Gasses = {'Gas', 'Symbol', 'Attraction(a)', 'Size';
    'Helium', 'He', 0.0341, 0.0237;
    'Hydrogen', 'H2', 0.244, 0.0266;
    'Oxygen', 'O2', 1.36, 0.0318;
    'Chlorine', 'Cl2', 6.49, 0.0562;
    'Carbon Dioxide', 'CO2', 3.59, 0.0427};

R = 0.08206; V = 20; T = 300;

Pressures = ((R*T)./(V-[Gasses{2:end,3}])) - ([Gasses{2:end,4}]./(V^2))

% Output
% ------
% Prob6_2
% Pressures =
%     1.2329    1.2460    1.3206    1.8221    1.5001