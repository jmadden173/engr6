% Prob6_1.m

% Part A
Data = {'Motor 28C', 'Text ID 6';[2,8;6,5],[4,3,2]}

% Part B
Data(2,1) = {[2,8;6,4]}

% Part C
Data{2,1} = [2,8;6,4]

% Part D
Data{2,1}(1,1)

% Output
% ------
% Prob6_1
% Data =
%   2�2 cell array
%     'Motor 28C'     'Text ID 6' 
%     [2�2 double]    [1�3 double]
% Data =
%   2�2 cell array
%     'Motor 28C'     'Text ID 6' 
%     [2�2 double]    [1�3 double]
% Data =
%   2�2 cell array
%     'Motor 28C'     'Text ID 6' 
%     [2�2 double]    [1�3 double]
% ans =
%      2