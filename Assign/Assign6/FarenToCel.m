% Prob6_4.m
% Name:         FarenToCel.m
% Purpose:      Convert Fahrenheit(F) to Celsius(C)
% Input:
%               Fahrenheit: a numerical array in degrees Farenheit
% Output:
%               Celsius:    a numerical array in degress Celsius
% Reference:    Problem Number 6-4 on assignment 6
% Author:       John Madden
% Date Created: 9/17/2018

% Sample Code
% FarenToCel(32)
% ans =
%      0

function Celsius = FarenToCel(Fahrenheit)
    Celsius = (5/9).*(Fahrenheit - 32);
end

% Output
% ------
% FarenToCel(60)
% ans =
%    15.5556