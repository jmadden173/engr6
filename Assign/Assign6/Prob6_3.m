% Prob6_3.m

load('N:\\ENGINEERING\\Engr6\\Math1B.mat');

Grades = cell2mat(ClassList(:,2:end))*Weights';
[BestScore, BestStudent] = max(Grades);
disp(['The best student is ', ClassList{BestStudent,1}, ' with a score of ', num2str(BestScore), '%']);

% Output
% ------
% Prob6_3
% The best student is Rothas with a score of 93.88%