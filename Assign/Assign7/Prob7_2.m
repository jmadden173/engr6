% Prob7_2.m
% Name:         fenceCalculation(Width, Area)
% Description:  Takes width and area and performs basic algebra
%               to solve for the length and total perieter of the
%               the area.
% Inputs:
%       Width:      The width of the fence around an enclosure, 
%                   A single number in meters.
%       Area:       The area of the fence around an enclosure,
%                   A single number in meters squared
% Outputs:
%       Length:     The length of the fence around an enclosure,
%                   A single number in meters
%       Perimeter:  The total perimieter of the fence around an
%                   enclosure. A single number in meters
% Date:         9/17/2018
% Author:       John Madden

% Sample:
%         ans =
%            11.8333
%         [Length, Perimeter] = Prob7_2(6, 80)
%         Length =
%            11.8333
%         Perimeter =
%            38.1519

function[Length, Perimeter] = fenceCalculation(Width, Area)
    Length = (Area / Width) - (1/4)*Width;
    Perimeter = (Length * 2) + Width + (sqrt(2) * Width);
end

% Output
% ------
% [Length, Perimeter] = Prob7_2(6, 80)
% Length =
%    11.8333
% Perimeter =
%    38.1519