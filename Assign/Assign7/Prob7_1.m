% Prob7_1.m
% Description:  Caclcuates the corresponding force F1
%               given F2, belt angle, and friction
%               coefficient.
% Reference:    EGNR 6, Assignment 7 Problem 1
% Name:         John Madden
% Date:         9/18/2019

BeltAngle = input('Enter in belt angle: ');
FrictionCoefficient = input('Enter the friction coefficient: ');
Force2 = input('Enter in Force 2 (F2): ');

Force1 = Force2 * exp(FrictionCoefficient * deg2rad(BeltAngle))

% Output
% ------
% Enter in belt angle: 130
% Enter the friction coefficient: .3
% Enter in Force 2 (F2): 100
% Force1 =
%   197.5217