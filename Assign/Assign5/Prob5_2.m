% File:     Prob5_2.m
% Purpose:  Used to solve for the cost, length, and radius of the
%           enclosure with the lowest cost using input from the
%           user. Refer to assignment 5 problem 2.
% Inputs:
%   FENCE_COST_STRAIGHT:    Is the cost for each foot of fence on the
%                           curved section of the enclosure
%   FENCE_COST_CURVE:       Is the cost for each foot of cence on the
%                           straight portion of the enclosure
%   AREA_REQUIRED:          Is the area required by the users enclosure.
%                           Units are in ft^2
% Outputs:
%   Length:                 The length of the long side of the enclosure
%                           in ft
%   Radius:                 The radius of the semicircle on the side of
%                           of the enclosure
%   Cost:                   The total cost of fence for the enclosre
%                           in $
% Author:   John Madden
% Date:     9/12/2018


FENCE_COST_STRAIGHT = input('Fence cost pre foot for straight: '); % per foot
FENCE_COST_CURVE =    input('Fence cost per foot for curve   : '); % per foot
AREA_REQUIRED =       input('Area of the enclosure           : '); % in ft^2

radius = 0:.01:100;
Length = (AREA_REQUIRED - ((pi.*(radius.^2))./2))./(2.*radius);

LengthIndex = Length > 0; % cannot have a side length of negative or zero

PossiblePerimeterBox = 2.*Length(LengthIndex) + 2.*radius(LengthIndex);
PossiblePerimeterCircle = pi.*radius(LengthIndex);

PossibleCosts = PossiblePerimeterBox.*FENCE_COST_STRAIGHT + PossiblePerimeterCircle.*FENCE_COST_CURVE;

[BestCost, BestCostIndex] = min(PossibleCosts);

disp('---------------------------------------------');
disp('       Length       Radius        Cost       ');
disp([Length(BestCostIndex), radius(BestCostIndex), BestCost]);
disp('---------------------------------------------');


% Prob5_2
% Fence cost pre foot for straight: 30
% Fence cost per foot for curve   : 40
% Area of the enclosure           : 1600
% ---------------------------------------------
%        Length       Radius        Cost       
%        28.371        18.61       5157.5
% ---------------------------------------------