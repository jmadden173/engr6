% File:     Prob5_1.m
% Purpose:  Used to solve for work and display in a table format.
%           Refer to assingment 5 problem 1
% Inputs:
%   Force:      A vector containing forces in newtons
%
%   Distance:   A vector containing distances in meters and the same
%                size as $Force vector
% Outputs:  A table containing Foce, Distance and work
% Author: John Madden
% Date 9/13/2018

format short G

Force = [400,550,700,500,600];
Distance = [3,0.5,0.75,1.5,5];
Work = Force .* Distance;

disp('          Force       Distance     Work');
disp([Force', Distance', Work']);

% Prob5_1
%           Force       Distance     Work
%           400            3         1200
%           550          0.5          275
%           700         0.75          525
%           500          1.5          750
%           600            5         3000