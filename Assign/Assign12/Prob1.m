% Name:         Prob1.m
% Description:  Calulates the pressure of a gas in a
%               container. Uses a constant set of
%               variables for calculating it
% Input:        v: a single number in L
%               t: a single number in kelvin
% Author:       John Madden
% Date:         10/9/2018

CONST_R = 0.08206;
CONST_GAS = {'Gas', 'Symbol', 'Attraction(a)', 'Size(b)';
                'Helium', 'He', 0.0341, 0.0237;
                'Hydrogen', 'H2', 0.244, 0.0266;
                'Oxygen', 'O2', 1.36, 0.0318;
                'Chlorine', 'Cl2', 6.49, 0.0562;
                'Carbon Dioxide', 'CO2', 3.59, 0.0427};
            
v = input('Input the specific volume (L/mol): ');
t = input('Inter the temperature    (Kelvin): ');

selection = menu('Select the type of gas', CONST_GAS{2:end,1}) + 1;

P = ((CONST_R * t)/(v - CONST_GAS{selection, 4})) - (CONST_GAS{selection, 3}/(v^2))

% Output
% ------
% Prob1
% Input the specific volume (L/mol): 20
% Inter the temperature    (Kelvin): 300
% P =
%     1.2181