% Name:         Prob2.m
% Description:  Simulate the financial accumulation
%               in bank accounts with regular deposits
%               and a yearly transfer to certificate of
%               deposit to earn extra interest
% Author:       John Madden
% Date:         10/9/2018

interest = 0.04;
cdinterest = 0.06;
bal = 0;
cdbal = 0;

deposits = [200,250,-100,350,400];

for i=1:5
    for j=1:12
        bal = bal*(1+(interest/12));
        bal = bal + deposits(i);
    end
    
    cdbal = cdbal*(1+(cdinterest));
    if (bal > 3000)
        bal = bal - 2000;
        cdbal = cdbal + 2000;
    end
end

disp(['Your balence is: ', num2str(bal)]);
disp(['Your cdbal is  : ', num2str(cdbal)]);

% Output
% ------
% Prob2
% Your balence is: 7993.6247
% Your cdbal is  : 6502.032