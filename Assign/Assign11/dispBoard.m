% Name:         dispBoard.m
% Description:  Takes a tictactoe game board
%               and displays it in ascii art
%               using disp
% Input:        node: a 3x3 matrix repesenting
%                   the playing board of a tictactoe
%                   game
% Output:       n/a
% Author:       John Madden
% Date:         10/9/2018

function dispBoard(board)
    disp([' ', num2str(board(1,1)), ' | ', num2str(board(1,2)), ' | ', num2str(board(1,3)), ' ']);
    disp('---+---+---');
    disp([' ', num2str(board(2,1)), ' | ', num2str(board(2,2)), ' | ', num2str(board(2,3)), ' ']);
    disp('---+---+---');
    disp([' ', num2str(board(3,1)), ' | ', num2str(board(3,2)), ' | ', num2str(board(3,3)), ' ']);
end