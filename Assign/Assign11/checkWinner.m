% Name:         checkWinner.m
% Desscription: Check if a player has won at
%               a game of tictactoe. Checks the
%               board for 0's and 1's using all
%               possible combinations of winning
%               plays
% Input:        node: a 3x3 matrix repesenting
%                   the playing board of a tictactoe
%                   game
% Output:       isWinnerNode: a integer representing
%                   the winning players
%                       0=nobody
%                       1=computer
%                       2=user
% Author:       John Madden
% Date:         10/9/2018

% Personal Notes:
% Outputs the value of the winning player
% 0 if no one is a winner
function [isWinnerNode] = checkWinner(node)
    isWinnerNode = 0;
    % Check if there are winners
    for i = 1:2
        % Look for horizontal wins
        for j = [1,4,7]
           if node(j) == i && node(j+1) == i && node(j+2) == i
              isWinnerNode = i;
           end
        end
        
        % Look for vertical wins
        for k = [1,2,3]
           if node(k) == i && node(k+3) == i && node(k+6) == i
               isWinnerNode = i;
           end
        end
        
        % Look for diagnals
        % Top left to bottom right
        if node(1) == i && node(5) == i && node(9)
            isWinnerNode = i;
        end
        % Top right to bottom left
        if node(3) == i && node(5) == i && node(7) == i
            isWinnerNode = i;
        end
    end
end