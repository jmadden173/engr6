% Prob11_3
% Description:	This script calculates the force to push
%				a object on a certain material. Takes the
%				type of material and outputs the force
%				required
% Input:		W, a linear scalar, weight of the object
%				choice, a linear scalar, corresponds the
%					the CONST_MATERIALS variable
% Output:		F, a linear scalar, force required to push 
%					the object
% Author:		John Madden
% Date:			10/7/2018

CONST_MATERIALS = {'Metal on metal', 0.20; 'Wood on wood', 0.35; 'Metal on wood', 0.40; 'Rubber on concrete', 0.70};

W = input('Enter in the weight of the material: ');

choice = menu('Choose the contact material', 'Metal on metal', 'Wood on wood', 'Metal on wood', 'Rubber on concrete');

F = [CONST_MATERIALS{choice, 2}]*W

% Output
% ------
% Prob3
% Enter in the weight of the material: 40
% F =
%      8