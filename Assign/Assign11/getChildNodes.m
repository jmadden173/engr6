% Name:         getChildNodes.m
% Description:  Gets all possible board derived from
%               a input board with a depth of 1. 
%               The algorithim will handle creation of
%               boards with greater depth
% Input:        node: a 3x3 matrix repesenting
%                   the playing board of a tictactoe
%                   game
%               player: a numerical value assinged to each
%                   of the players
% Output:       childnodes: a 3x3xN matrix where N is the
%                   number of possible boards derived from
%                   the input.
% Author:       John Madden
% Date:         10/9/2018

function [childnodes] = getChildNodes(currentnode, player)
    childnodes = [];
    index = 1;
    % Row
    for i=1:3
        % Collumn
        for j=1:3
            if currentnode(i,j)==0
               childnode = currentnode;
               childnode(i,j)=player;
               childnodes(:,:,index) = childnode;
               index = index + 1;
            end
        end
    end
end