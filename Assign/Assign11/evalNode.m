% Name:         evalNode.m
% Description:  Assigns a numerical value
%               if a plyers has won or not.
%               Used for computing the best
%               possible solution to tictactoe
%               using minimax algortithim
% Input:        node: a 3x3 matrix repesenting
%                   the playing board of a tictactoe
%                   game
% Output:       n/a
% Dependencies: checkWinner.m
% Author:       John Madden
% Date:         10/9/2018

function value = evalNode(node)
    winner = checkWinner(node);
    if (winner == 1)
        value = -1;
    elseif (winner == 2)
        value = 1;
    else
        value = 0;
    end
end