% Name:         CheckTerminalNode.m
% Description:  Checks if the entire board
%               (3x3 matrix) is filled with
%               either 1's or 2's representing
%               plays on the board
% Input:        node: a 3x3 matrix repesenting
%                   the playing board of a tictactoe
%                   game
% Output:       isTerminal: either a 1 or a 0 going 
%                   by the boolean standard
% Author:       John Madden
% Date:         10/9/2018

% Checks if the entire board is full
% 0 = it is not a terminal node
% 1 = it is a terminal node
function [isTerminalNode] = checkTerminalNode(node)
    isTerminalNode = sum(sum(node == zeros(3,3)));
    if (sum(node) == 0)
        isTerminalNode = 0;
    end
end