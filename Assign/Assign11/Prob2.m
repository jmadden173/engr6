% Name:			Prob2
% Description:	This script is used to compute the
%				maximum height or minimum velocty
%				or the time to hit. It takes the
%				initial velocity and angle as inputs.
%				It uses a constant gravity of 9.81
%				meters^2/sec
% Input:		v0: a linear scalar in m/s, initial velocity
%				ang: a linear scalar in degrees, angle
% Output:		hmax, a linear scalar, max height
%				vmin, a linear scalar, min velociy
%				thit, a linear scalar, time to hit
% Author:		John Madden
% Date:			10/7/2018

choice = menu('Choose what to compute', 'h(max)', 'v(min)', 't(hit)');
v0 = input('Enter the initial velocity in meters/sec: ');
ang = input('Enter the angle in degrees: ');

g = 9.81;

switch choice
	case 1
		hmax = ((v0^2)*(sind(ang)^2))/(2*g)
	case 2
		vmin = v0*cosd(ang)
	case 3
		thit = 2*(v0/g)*sind(ang)
end

% Output
% ------
% Test for h(max)
% Prob2
% Enter the initial velocity in meters/sec: 40
% Enter the angle in degrees: 30
% hmax =
%    20.4082
%
% Test for v(min)
% Prob2
% Enter the initial velocity in meters/sec: 40
% Enter the angle in degrees: 30
% vmin =
%    34.6410
%
% Test for t(hit)
% Prob2
% Enter the initial velocity in meters/sec: 40
% Enter the angle in degrees: 30
% thit =
%     4.0775