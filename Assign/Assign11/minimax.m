% Name:         minimax.m
% Description:  Uses a minimax algorithim to
%               find the best possible move in
%               a game of tictactoe. Done with
%               recursion
% Input:        node: a 3x3 matrix reprenting the game board
%                   0 represents an empty space
%                   1 represents the computer move
%                   2 represents the users move
%               depth: the max depth for the algrothim to search
%                   through, recomended is 10
%               maximizingPlayer: a zero or a one that represents
%                   if it is the users move or the computers move
% Output:       nodevalue: a value that represents how good a move it,
%                   the more negative the better
% Requires:     getChildNodes.m
%               checkWinner.m
%               checkTerminalNode.m
%               evalNode.m
% Author:       John Madden
% Date:         10/9/2018

% Personal Notes:
% node = struct with child element
% depth = integer
% maximizngPlayer = bool
% 1 = maximizing player
% 0 = minimiazng player
%
% 0 = empty space
% 1 = computer
% 2 = player

function [nodevalue] = minimax(node, depth, maximizingPlayer)
    if (depth == 0 | checkTerminalNode(node) == 1 | checkWinner(node) ~= 0)
        nodevalue = evalNode(node);
        return
    end
    
    if (maximizingPlayer == 1)
        nodevalue = -1 * Inf;
        child = getChildNodes(node, 1);
        for i = size(child, 3)
            nodevalue = max(nodevalue, minimax(child(:,:,i), depth - 1, 0));
        end
        return
    else
        nodevalue = Inf;
        child = getChildNodes(node, 2);
        for i = size(child, 3)
            nodevalue = min(nodevalue, minimax(child(:,:,i), depth - 1, 1));
        end
        return
    end
end