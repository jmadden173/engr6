% filename:     Prob1.m
% Description:  Solves an optimization problems for different products
%               requiring different hours of work on different machines
% Output:       Outputs the optimum amount of widget and the max
%               profit that Acme Spinning Wheel Company can produce
% Author:       John Madden
% Date:         10/18/2018

clear;

Hours = [40 30 45]';  % of Lathe, Ginder, Miling
UnitProfit = [100 150 90 120];             % for Treadle, Whirl, Bobbin, Flyer
Require = [1 2 0.5 3; 0 2 4 1; 3 1 5 2];
StepSize = 1;
MaxProfit = 0;
Solution = [];
tic
for Treadle = 0:StepSize:min(Hours./Require(:,1))
   for Whirl = 0:StepSize:min(Hours./Require(:,2)) 
      for Bobbin = 0:StepSize:min(Hours./Require(:,3))
          for Flyer = 0:StepSize:min(Hours./Require(:,3))
            if  Require*[Treadle Whirl Bobbin Flyer]' <= Hours  % if has built in all
               Profit = [Treadle Whirl Bobbin Flyer]*UnitProfit';    
               if Profit > MaxProfit
                  MaxProfit = Profit;
                  Solution = [Treadle Whirl Bobbin Flyer Profit];
               elseif Profit == MaxProfit
                  Solution = [Solution ; [Treadle Whirl Bobbin Flyer Profit]];
               end
            end
          end
      end
   end
end
toc
disp('Optimum number of widgets')
disp('         Treadle     Whirl     Bobbin     Flyer     Net Profit')
disp(Solution)


% Output
% ------
% Prob1
% Elapsed time is 0.068934 seconds.
% Optimum number of widgets
%          Treadle     Whirl     Bobbin     Flyer     Net Profit
%           10          15           0           0        3250

