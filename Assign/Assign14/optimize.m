function [outProfit, itemCounter] = optimize(hoursAvail, profit, itemCount)
    hoursReq = [1,2,0.5,3;0,2,4,1;3,1,5,2];
    unitProfit = [100,150, 90, 120];

    for Part=1:4
        % Subtracts the hours needed to produce a part
        newHoursAvail = hoursAvail - hoursReq(:,Part)';
        newProfit = profit + unitProfit(Part);
        newItemCount = itemCount;
        newItemCount(Part) = newItemCount(Part) + 1;
        
        if newHoursAvail > 0 
            outProfit = optimize(newHoursAvail, newProfit, newItemCount);
        else
            outProfit = profit;
            break
        end
    end
end