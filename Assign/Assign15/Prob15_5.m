% Prob15_5
% Description:  Generates a plot of volatages of a variable point
%               relative to two other static points
% Input:        loc, Locations of the static points
%               q1,q2, charge values of static poitns
%               e, epsilon, a constant in the function
%               r, range of x and y values
% Name:         John Madden
% Date:         10/22/2018

loc = [0.3, 0;-0.3, 0];

q1 = 2 * (10 ^ -10); q2 = 4 * (10 ^ -10);
q = [q1, q2];
e = 8.854 * (10 ^ -12);

r = -0.25:.01:0.25;

[mesh_r1, mesh_r2] = meshgrid(r);

d1 = sqrt((mesh_r1 - loc(1,1)).^2 + (mesh_r2 - loc(1,2)).^2);
d2 = sqrt((mesh_r1 - loc(2,1)).^2 + (mesh_r2 - loc(2,2)).^2);

V = (1/(4*pi*e)) .* ((q1./d1) + (q2./d2));

mesh(mesh_r1, mesh_r2, V);

xlabel('X Location (m)');
ylabel('Y Location (m)');
zlabel('Volages (v)');

title('Electric Potential Voltage V at a point due to two charges');

AxHandle = gca;
axes('position',[0 0 1 1],'visible','off')
text(.95,.85,{'SRJC Engr 6';'John Madden';'10/22/2018'},...
   'HorizontalAlignment','right')
text(.02,.88,{['Charges:      ',num2str(q,'%8.0E'),' (C)'];
    ['X Locations:  ',num2str(loc(1,:)), ' (m)'];
    ['Y Locations:  ',num2str(loc(2,:)), ' (m)']})
axes(AxHandle)