% Proab15_4.m

Gasses = struct(...
    'Gas', {'Helium', 'Hydrogen', 'Oxygen', 'Chlorine', 'Carbon Dioxide'},...
    'Symbol', {'He', 'H2', 'O2', 'Cl2', 'CO2'},...
    'Attraction', {0.0341, 0.244, 1.36, 6.49, 3.59},...
    'Size', {0.0237, 0.0266, 0.0318, 0.0562, 0.0427}...
    );

R = 0.08206; V = 20; T = 300;

Pressures = (((R*T)./(V-[Gasses.Size]')) - ([Gasses.Attraction]'./(V^2)))'

% Output
% ------
% Prob15_4
% Pressures =
%     1.2329    1.2460    1.3206    1.8221    1.5001