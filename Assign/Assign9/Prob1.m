v0 = 40; angle = 30; gravity = 9.81;

time = 0:.001:(2*(v0/gravity)*sind(angle));

height = (v0 .* time .* sind(angle)) - (0.5 .* gravity .* (time.^2));
velocity = sqrt((v0.^2) - (2 .* v0 * gravity .* time .* sind(angle) + (gravity.^2) .* (gravity^2)));

timelimited = time(height >= 15 & velocity <= 36);

disp(['The min of the time when height is greater than 15 and the velocity is less than 35 is ', num2str(min(timelimited)), ' and the max is ', num2str(max(timelimited))])

% Output
% The min of the time when height is greater than 15 and the velocity is less than 35 is 0.991 and the max is 3.086
