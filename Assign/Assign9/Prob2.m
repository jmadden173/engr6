x = 1:11

% Part a
%x>=10 | x<6
%x~=2 & x<=5

% Part b
disp('Comparing ~(x<10 & x>=6) to x>=10 | x<6');
~(x<10 & x>=6)
x>=10 | x<6

fprintf('\n\n');

disp('Comparing ~(x==2 | x>5) to x~=2 & x<=5');
~(x==2 | x>5)
x~=2 & x<=5

% Output
% Prob2
% x =
%      1     2     3     4     5     6     7     8     9    10    11
% Comparing ~(x<10 & x>=6) to x>=10 | x<6
% ans =
%   1�11 logical array
%    1   1   1   1   1   0   0   0   0   1   1
% ans =
%   1�11 logical array
%    1   1   1   1   1   0   0   0   0   1   1
% 
% 
% Comparing ~(x==2 | x>5) to x~=2 & x<=5
% ans =
%   1�11 logical array
%    1   0   1   1   1   0   0   0   0   0   0
% ans =
%   1�11 logical array
%    1   0   1   1   1   0   0   0   0   0   0
