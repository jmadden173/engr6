% Prob3.m
% Description:      Calcualtes the the distance based on arguments of
%                   Force (W), SpringConstant1 (k1), SpringConstant2
%                   (k2), and distance (d), for a scale
% Input:            W, the Weight of the object on the scale, a scalar
%                   k1, the Spring constant for the longer spring, a scalar
%                   k2, the Spring constatn for the shorter 2 springs, a
%                       scalar
%                   d, distance between the two springs, a scalar
% Output:           x, the distance that the platform moves toward the
%                       ground
% Author:           John Madden
% Date:             10/3/2018

function [x] = compdist(W, k1, k2, d)
    x = W/k1;
    
    if x > d
        x = (W + 2 * k2 * d)/(k1 + 2 * k2);
    end
end

% Output
% Prob3(500, 10^4, 1.5*(10^4), .1)
% ans =
%     0.0500

% Prob3(2000, 10^4, 1.5*(10^4), .1)
% ans =
%     0.1250