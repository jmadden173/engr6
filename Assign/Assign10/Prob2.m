% Filename:     Prob2.m
% Purpose:      Takes a numerical grade value and convert it into
%               a corresponding letter grade
% Input:        numgrade, numerical scalar, between 0 and 100
% Output:       chargrade, character, either A,B,C,D,F
% Author:       John Madden
% Date:         10/1/2018

numgrade = input('Input the numerical grade in percent: ');

A=89.5; B=79.5; C=69.5; D=59.5;

if numgrade > A
    chargrade = 'A';
elseif numgrade > B
    chargrade = 'B';
elseif numgrade > C
    chargrade = 'C';
elseif numgrade > D
    chargrade = 'D';
elseif numgrade <= D && numgrade >= 0
    chargrade = 'F';
else
    disp('You did not enter a value between 0 and 100');
    return
end

disp(['Your score of ', num2str(numgrade), ' earned a ', chargrade]);

