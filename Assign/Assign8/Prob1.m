% Prob1
x = [-3,0,0,2,6,8];
y = [-5,-2,0,3,4,10];

% Part a
indexes = find(x>y)

% Part b
values = x(indexes)

% Prob1
% indexes =
%      1     2     5
% values =
%     -3     0     6