% Prob2
PriceA = [ 19, 18, 22, 21, 25, 19, 17, 21, 27, 29];
PriceB = [ 22, 17, 20, 19, 24, 18, 16, 25, 28, 27];
PriceC = [ 17, 13, 22, 23, 19, 17, 20, 21, 24, 28];

% Part a
Parta = sum((PriceA > PriceB) & (PriceA > PriceC))

% Part b
Partb = sum(xor((PriceA > PriceB),(PriceA > PriceC)))

% Prob2
% Parta =
%      4
% Partb =
%      5