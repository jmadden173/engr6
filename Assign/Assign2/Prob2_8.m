% Part a
% Calculating Pressure
%----------------------
R = 0.08206; n = 1; V = 22.41; T = 273.2;
P = (n*R*T)/V

% Part b
% Calculate using van der
% Waals Correction factor
%-------------------------
a = 6.49; b = .0562;
P_corrected = (n*R*T)/(V-(n*b))-((a*(n^2))/(V^2))

% Prob2_8
% P =
%     1.0004
% P_corrected =
%     0.9900
