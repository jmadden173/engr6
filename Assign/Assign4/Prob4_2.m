A = [3,-2,1;6,8,-5;7,9,10];
B = [6,9,-4;7,5,4;-8,2,1];
C = [-7,-5,2;10,6,1;3,-9,8];

% Part A
D = cat(3, A, B, C)

% Part B
MaxPage = max(max(D))

% Part C
MaxOverall = max(max(max(D)))

% D(:,:,1) =
%      3    -2     1
%      6     8    -5
%      7     9    10
% D(:,:,2) =
%      6     9    -4
%      7     5     4
%     -8     2     1
% D(:,:,3) =
%     -7    -5     2
%     10     6     1
%      3    -9     8
% MaxPage(:,:,1) =
%     10
% MaxPage(:,:,2) =
%      9
% MaxPage(:,:,3) =
%     10
% MaxOverall =
%     10