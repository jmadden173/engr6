Price = [300,550,400,250,500];
QuantityPurchased = [5,3,6,3,2;3,2,5,5,4;6,4,3,4,3];

% Part A
Spent = Price.*QuantityPurchased

% Part B
SpentMonth = sum(Spent, 2)'

% Part C
SpentMaterials = sum(Spent,1)

% Part D
SpentTotal = sum(SpentMaterials)

% Spent =
%         1500        1650        2400         750        1000
%          900        1100        2000        1250        2000
%         1800        2200        1200        1000        1500
% SpentMonth =
%         7300        7250        7700
% SpentMaterials =
%         4200        4950        5600        3000        4500
% SpentTotal =
%        22250