ProductionCosts = [7,3,2;3,1,3;9,4,5;2,5,4;6,2,1] .* 1000;
ProductionVolumes = [16,14,10,12;12,15,11,13;8,9,7,11;14,13,15,17;13,16,12,18];

% Part A
QuarterlyCosts = sum(ProductionCosts,2).*ProductionVolumes

% Part B
YearlyCosts = sum(QuarterlyCosts,2)

% Part C
QuarterlyCostsTotal = sum(QuarterlyCosts)

% QuarterlyCosts =
%       192000      168000      120000      144000
%        84000      105000       77000       91000
%       144000      162000      126000      198000
%       154000      143000      165000      187000
%       117000      144000      108000      162000
% YearlyCosts =
%       624000
%       357000
%       630000
%       649000
%       531000
% QuarterlyCostsTotal =
%       691000      722000      596000      782000